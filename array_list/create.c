#include"list.h"

list *create(int *len) {
	list *l = (list*)malloc(sizeof(list));
	if (l == NULL) return;
	l->data = (int*)malloc(sizeof(int)*(*len));
	l->len = 0;
	l->predel = *len;
	l->fence = 0;
	return l;
}