#pragma once
struct List;
typedef struct List list;

list *create(int*);
void destroy(list*);
void clear(list*);

int get_value(list*);
void set_pos(list*, int*);
void set_start(list*);
void set_end(list*);

void next(list*);
void prev(list*);

void append(list*, int*);
void insert(list*, int*);
void remove(list*, int*);